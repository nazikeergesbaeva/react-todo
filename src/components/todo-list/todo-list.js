import React from 'react';

import TodoListItem from '../todo-list-item';
import './todo-list.css';


const TodoList = ({ todos, onImportantChange, onTodoDelete }) => {

  const elements = todos.map((item) => {
    return (
      <li key={item.id} className="list-group-item">
        <TodoListItem {...item } onImportantChange={onImportantChange} onTodoDelete={onTodoDelete} />
      </li>
    );
  });

  return (
    <ul className="list-group todo-list">
      { elements }
    </ul>
  );
};

export default TodoList;
